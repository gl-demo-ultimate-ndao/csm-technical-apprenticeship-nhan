## Week 4 Goals
- [ ] Create a compliance pipeline
- [ ] Create a compliance framework and add it to your Ultimate project
- [ ] Create a scan result policy and add it to your Ultimate project
- [ ] Introduce a vulnerability via a merge request
- [ ] Resolve a vulnerability

## Async Work
<details><summary> Click to expand </summary>

### The GitLab Flow - Secure
![Screen_Shot_2023-01-10_at_4.34.51_PM](/uploads/e7f5238f3d4737008174c78df04272d8/Screen_Shot_2023-01-10_at_4.34.51_PM.png)

This is the recommended flow we provide to customers. We are primarily focusing on the `Security Scans` and `Security Dashboard` portion of this flow. You may notice that `Security Scans` occurs in more than one area of our GitLab flow. This is because it is best practice to run security scans upon every commit to a merge request on your feature branch, as well as on your main branch after merging your feature branch into main. This way you can see new vulnerabilities introduced in the merge request, as well as any that exist in production.

### Vulnerabilities
What is a vulnerability? Vulnerabilities indicate potential areas of insecurity in your application. They are individual records that have various details to them, including:

- Description
- Time the vulnerability was detected
- Location where the vulnerability was detected
- Status
- Suggested fixes (if available)
- Linked issues
- Security training for a vulnerability (off by default)
- Actions log

Vulnerabilities are detected upon security scans running and the jobs succeeding. A vulnerability record is automatically created upon a security scan. You can generate an a pre-filled issue from a vulnerability to further documentation, labeling, assigning work, and creating MRs to resolve the vulnerability.

#### View Vulnerabilities in the Merge Request
You can preview vulnerabilities in the Merge Request Widget, as new vulnerabilities can be detected with every MR pipeline run. These vulnerabilities can then be actioned by viewing the vulnerability record, locating the vulnerability in the code, and committing a change to resolve the vulnerability. Seeing these vulnerabilities prior to merging code into the default branch can prevent vulnerable code from entering production.

#### Vulnerability Report
The vulnerability report is viewable at the project and group level. It shows the vulnerabilities that were detected in the default branch of the project only, not feature branches. The vulnerability report allows you to see all vulnerabilities in production at a glance, as well as filter by their severity, scan type, and more. You can drill into specific vulnerability records and create issues from them for vulnerabilities that require some code changes to manage. Then, you can create an MR from the vulnerability issue to resolve the vulnerability.

### Security Scans
GitLab has both static and dynamic security scans. Static scans are a form of white-box testing - they detect vulnerabilities in source code (for example, SAST and Secret Detection). Dynamic scans are a form of black-box testing - they scan running applications (for example, DAST). Both are paramount to improving your customers' security postures, as they serve two distinct but unified purposes.

### Security Dashboard
The Security Dashboard shows health scores of projects by assigning a grade A-F to the project depending on the presence of some severity vulnerability. The Security Dashboard is available at the group and instance level, however, at the instance level the Admin has to add projects of interest to track and can only see the dashboard configured by them on their account. The Security Dashboard is helpful for tracking trends over time and overall security posture.

### Our Competitors
Some of our top Secure competitors are:
- AppScan
- Checkmarx
- Snyk
- Sonarqube
- Synopsys
- Veracode

## Governance and Separation of Duties
To complement the Secure stage, we have the Govern stage. This encompasses various features that promote the "separation of duties" in a GitLab instance or namespace.

### What is "separation of duties"?
Separation of duties is the concept of having more than one person required to complete a task. This allows for there to be no "single points of failure" in a process, and for there to be more than one role involved in developing and deploying software. This is incredibly important in ensuring that all the vulnerability findings found in the development process gate the approval and deployment process, and security is involved in an otherwise highly automated and fast-paced software lifecycle.

### [Compliance Pipelines](https://docs.gitlab.com/ee/user/group/compliance_frameworks.html#configure-a-compliance-pipeline)
Compliance pipelines are very similar to standard CI/CD pipelines, as their configuration files use the same syntax as the .gitlab-ci.yml file. Typically the file is named .compliance-gitlab-ci.yml, and the file is located in a project outside of the project used for developing code that houses the .gitlab-ci.yml file. This separates out the two pipeline files so that developers who have access to the development project do not have access to the project housing the compliance pipeline configuration file. 

By default, the compliance pipeline configuration will take precedence over the project's pipeline configuration, so you must include the project pipeline file in the compliance pipeline file with the include keyword to ensure both run seamlessly together. Additionally, all stages in the project's .gitlab-ci.yml must exist in the compliance pipeline file, but the compliance pipeline file can have more stages listed than the .gitlab-ci.yml file.

Typically you will configure your security jobs in the .compliance-gitlab-ci.yml file and then associate it with a compliance framework that is added to a development project. This way security jobs are managed outside of the development project, asserting scans must run on all projects.

### Compliance Framework
The compliance framework is the label that is applied to a project to associate it with some compliance standard. It is essentially a label created at the group level that can be associated with a project. You can now have a default compliance framework that is automatically assigned to all new projects in your group. 

You can associate a compliance pipeline file with a compliance framework, and then associate the framework with a development project. This is what links the .compliance-gitlab-ci.yml file with the development project.

### Scan Execution Policies
Scan Execution Policies allow for scanning at a specific cadence. These policies that control the scan execution live in a separate policy project and the policy project is associated with the development project. Scan execution policies are different from compliance pipelines, as they are better for jobs that have project specific variables that need to be set, versus jobs that could apply to all projects in a group that are better suited for a compliance pipeline.

### Scan Result Policies
Scan Result Policies are also managed in an external project. These policies specify when to require approval from a user or group of users on a merge request. For example, you can create a policy that requires approval from a group of security engineers if a critical vulnerability is detected after a merge request pipeline run. This policy effectively gates new vulnerabilities from entering into your production code base.

</details>

## Exercise: Secure and Govern Your Blog

<details><summary>Click to expand</summary>

- Create a compliance pipeline
   - [ ] Create a new project in your group and call it `Compliance`. Leave all settings as default
   - [ ] In this project, create a new file called `.compliance-gitlab-ci.yml`
   - [ ] In this file, add the [Week 4 snippet code](https://gitlab.com/smorris-secure-app-demo1/ultimate-demo-template/-/snippets/2510193)
- Create a compliance framework and add it to your Ultimate project
   - [ ] Navigate to your group
   - [ ] Navigate to your Settings -> Compliance framework and select `Expand` and `Add framework`
   - [ ] Create a new compliance framework and associate the compliance pipeline file you just created: `.compliance-gitlab-ci.yml@gl-demo-ultimate-{YOUR_FIRST_INITIAL_LAST_NAME}/compliance`
   - [ ] Fill in remaining values and select `Save changes`
   - [ ] Navigate to your main project and select Settings -> Compliance framework and select Choose your framework to access your new compliance framework
   - [ ] Apply the framework to the project by clicking `Save changes`
   - [ ] Navigate to CI/CD -> Pipelines and select `Run pipeline`, leave values empty and then select `Run pipeline`
   - [ ] Verify that the test stage has `license_scanning`, `secret_detection`, and `semgrep_sast` jobs running
- Create a scan result policy and add it to your Ultimate project
   - [ ] Navigate to your project then Security and Compliance -> Policies
   - [ ] Select `New policy`
   - [ ] Select `Scan result policy`
   - [ ] Add a name and description to your policy
   - [ ] Configure your rule to if all scanners detect 0 or more critical vulnerabilities, require approval from @sam 
   - [ ] View the policy yml in the preview on the right side of the page to verify your policy
   - [ ] Select `Configure with merge request`
   - [ ] Note the project that was created to house the policy.yml file that the merge request was created in
   - [ ] If you see no changes, refresh the page and verify the policy file was created
   - [ ] Select `Merge`
   - [ ] Navigate to your project then Security and Compliance -> Policies
   - [ ] Verify the new scan result policy is listed
- Introduce a vulnerability via a merge request
   - [ ] Open a merge request in your main project and copy in the following line into a location of your choice in `index.html`, preferably between a `<p></p>` or `<h1></h1>` tag:
      - `glpat-JUST20LETTERSANDNUMB` 
   - [ ] Verify that secret detection succeeds and a vulnerability appears in the merge request widget
   - [ ] Verify that approval is now required from @sam to merge the merge request 

*Stop for now until the live discussion.*

</details>

## Next Steps
Live Discussion:
- Resolve the vulnerability together by removing the vulnerable line of code in the merge request

~content
